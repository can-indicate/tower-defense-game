# Tower Defense Game #

Fork of [TowerDefense](https://github.com/oxben/TowerDefense) by GitHub user [Oxben](https://github.com/oxben/TowerDefense).
This project also incorporates code and/or concepts from the following sources:

*  [godot-pathfinding2d-demo](https://github.com/FEDE0D/godot-pathfinding2d-demo) by GitHub user [FEDE0D](https://github.com/FEDE0D)
*  [Godot Dialogue Graph with Json!](https://insbilla.itch.io/branching-dialogue-graph-godot-with-json) by itch.io user [insbilla](https://insbilla.itch.io/)
*  [Touchscreen Camera](https://github.com/kidscancode/godot_recipes/blob/master/src/content/2D/touchscreen_camera.md) by [kidscancode](https://github.com/kidscancode)

We are in no way endorsed or connected with any of the above parties.

A live version of this game is hosted on [itch.io](https://can-indicate.itch.io/turret-demo?secret=bjvD8Zs2KEhI0ODqHwxWGZ6K6Vw). 
Note that the live version will not be as up to date as this repository.
