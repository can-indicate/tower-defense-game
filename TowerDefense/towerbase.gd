extends Node2D

onready var global = get_node("/root/global")
var spear = 1
var bow = 1
var mag1 = 1
var mag2 = 1
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("towerbase")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	for i in $Control.get_children():
		if i.tower != null:
			if i.tower.deck_name == "spear":
				i.tower.level = spear


func _on_button_pressed():
	var deck = global.deck
	if deck == "spear":
		spear += 1
	elif deck == "bow":
		bow += 1
	elif deck == "mag1":
		mag1 += 1
	elif deck == "mag2":
		mag2 += 1
	global.current_level.get_child(0).get_node("background").modulate = Color(1,1,1,1)
	$button.hide()
	global.current_level.get_node("card_deck/options").show()
	global.current_level.get_node("card_deck/decks").hide()
	global.current_level.get_node("card_deck/goback2").hide()
