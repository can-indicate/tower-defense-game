extends Control

var challenge_mode = false

onready var global = get_node("/root/global")

func load_save():
	var save_game = File.new()
	var save_dict = null
	if save_game.file_exists("res://game.save"):
		save_game.open("res://game.save", File.READ)
		save_dict = parse_json(save_game.get_line())
	save_game.close()
	return save_dict

func _ready():
	var level = _load_save_to_global_get_level()
	if level == 10:
		challenge_mode = true 
	print("Splash ready!")

func _on_start_pressed():
	_on_LevelSelectButton_pressed()

func _on_quit_pressed():
	get_tree().quit()
	
func _load_save_to_global_get_level():
	var level = 1
	var save_dict = load_save()
	var global = get_node("/root/global")
	if save_dict:
		level = save_dict["level"]
		global.game_won = save_dict["game_won"]
		global.health = save_dict["health"]
		global.cash = save_dict["cash"]
	return level

func _on_LevelSelectButton_pressed():
	global.mode = "STORY"
	global.health = 10
	var level = _load_save_to_global_get_level()
#	var global = get_node("/root/global")
	var level_select = ResourceLoader.load("res://level-select.tscn")
	var level_select_instance = level_select.instance()
	if global.game_won:
		level = 10
	level_select_instance.setup_level_buttons(level)
	add_child(level_select_instance)

func _on_load_pressed():
	global.mode = "CHALLENGE"
	global.health = 100
	var level = _load_save_to_global_get_level()
	if challenge_mode:
		global.goto_scene("res://challenge_mode.tscn")
#		if not str(level).is_valid_integer():
#			return
#		var global = get_node("/root/global")
#		global.goto_scene("res://game.tscn", "level-0" + str(level))
	else:
		get_node("PopupPanel").show()


func _on_Button_pressed():
	get_node("PopupPanel").hide()
