extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func animate(direction):
	if direction == "front":
		$front.show()
		$left.hide()
		$back.hide()
		$front/AnimationPlayer.play("attack")
	elif direction == "right":
		$front.hide()
		$left.hide()
		$back.hide()
		$left/AnimationPlayer.play("attack")
	elif direction == "back":
		$front.hide()
		$left.hide()
		$back.show()
		$back/AnimationPlayer.play("attack")
	elif direction == "left":
		$front.hide()
		$left.show()
		$back.hide()
		$left/AnimationPlayer.play("attack")
