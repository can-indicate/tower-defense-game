
extends RigidBody2D

onready var global = get_node("/root/global")

# Speed of the unit in pixel/sec
var speed = 100

# Number of hit points
var health = 1
# Number of Armor points
var armor = 2

# Number of damage points done by the unit when it reaches its goal
var damage = 1

# Number of coins gained when the unit is destroyed
var reward = 5

# Number of seconds since the unit died
var dead_since = 0

# A random number
var random_number = 0

var points = []
var points2 = []


func _ready():
	var current_level = int(global.print_levelname().substr(6,8))

#	random_number = randi() % 6 + 4
#	for i in range(4, 10):
#		if i != random_number:
#			get_children()[i].hide()
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var rand_num
	if 0 < current_level and current_level < 4:
		rand_num = rng.randi_range(1, 3)
		if rand_num == 1:
			$Stone1.show()
		elif rand_num == 2:
			$tree1.show()
		elif rand_num == 3:
			$boss1.show()
	elif 3 < current_level and current_level < 7:
		rand_num = rng.randi_range(1, 3)
		if rand_num == 1:
			$Stone2.show()
		elif rand_num == 2:
			$tree2.show()
		elif rand_num == 3:
			$boss2.show()
	elif 6 < current_level and current_level < 10:
		rand_num = rng.randi_range(1, 3)
		if rand_num == 1:
			$Stone3.show()
		elif rand_num == 2:
			$tree3.show()
		elif rand_num == 3:
			$boss3.show()
	else:
		rand_num = rng.randi_range(1, 9)
		if rand_num == 1:
			$Stone1.show()
		elif rand_num == 2:
			$Stone2.show()
		elif rand_num == 3:
			$Stone3.show()
		elif rand_num == 4:
			$tree1.show()
		elif rand_num == 5:
			$tree2.show()
		elif rand_num == 6:
			$tree3.show()
		elif rand_num == 7:
			$boss1.show()
		elif rand_num == 8:
			$boss2.show()
		elif rand_num == 9:
			$boss3.show()
			
	
	

	add_to_group("enemy")
	get_node("HealthLabel").set_text(str(health))
	var progress = get_node("HealthProgress")	
	progress.hide()
	progress.set_max(health)
	progress.set_value(health)
	set_physics_process(true)


func _physics_process(delta):
	if health <= 0:
		if dead_since > global.DEAD_CLEAN_INTVAL:
			queue_free()
		else:
			dead_since += delta
		return
		
	var eps = 1.5
	points = get_node("../Navigation2D").get_simple_path(
	self.get_global_position(), get_node("../TargetPoint1").get_global_position(), false)
	points2 = get_node("../Navigation2D").get_simple_path(
	get_global_position(), get_node("../TargetPoint2").get_global_position(), false)
	# var points_choice = points
	var points_choice = points if len(points) < len(points2) else points2
	if points_choice.size() > 1:
		var distance = points_choice[1] - get_global_position()
		var direction = distance.normalized() # direction of movement
		if distance.length() > eps or points_choice.size() > 2:
			set_global_position(get_global_position() + direction*speed*delta)
		else:
			print(get_name() + " reached the fortress")
			global.hit_fortress(damage)
			queue_free()
	
	
#func _draw():
#	# if there are points to draw
#	if points.size() > 1:
#		for p in points:
#			draw_circle(p - get_global_position(), 8, Color(1, 0, 0)) # we draw a circle (convert to global position first)


func hit(damage, continuous=false):
	if health <= 0:
		# If unit is already dead (case of wrecks)
		return
	if not continuous:
		health -= max(0, damage - armor)
	else:
		health -= damage
	get_node("HealthLabel").set_text(str(health))
	var progress = get_node("HealthProgress")
	if not progress.is_visible():
		progress.show()
	progress.set_value(health)
	# Death
	if health <= 0:
		print(get_name(), " destroyed!")
		get_children()[random_number].hide()
		progress.hide()
		# Add explosion
		var scene = preload("res://explosion-big.tscn")
		var explosion = scene.instance()
		explosion.set_position(get_global_position())
		get_node("/root").add_child(explosion)
		# Add wreckage
		scene = preload("res://wreck-a.tscn")
		var wreck = scene.instance()
		wreck.set_position(get_global_position())
		wreck.set_frame(randi() % wreck.get_hframes())
		var root = get_node("/root")
		# global.current_level.level.get_node("DetailsTileMap").add_child(wreck)

		var texture = ImageTexture.new()
		texture.load("res://assets/images/tank-a-dead.png")
		var sprite = get_node("Sprite")
		sprite.set_texture(texture)
		sprite.set_hframes(1)
		sprite.set_frame(0)
		remove_from_group("enemy")

		# Add label for reward
		scene = preload("res://ascending-label.tscn")
		var label = scene.instance()
		label.set_text("+ $" + str(reward))
		add_child(label)
		global.enemy_kill_reward()
#		global.increase_cash(reward)

