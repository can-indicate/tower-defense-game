
extends TextureRect

onready var global = get_node("/root/global")

var rand_num

var time
var spawn_time
var max_enemy_num = 8
var enemy_num = 0

var start_countdown = 10.0
var victory = false

# Current level scene
var level_name = ""
var level = null
# Enemy waves
var waves = []
# Current enemy wave
var wave_idx = 0
var wave = {}

const WAIT     = 0
const ACTIVE   = 1
const FINISHED = 2

const ARROW_CURSOR = "res://assets/images/cursor-arrow-32x32.png"
var arrow_cursor = null

const LAND_MINE_CURSOR = "res://assets/images/land-mine-32x32.png"
const OBSTACLE_CURSOR = "res://assets/images/obstacle-grass.png"
const FLOOR_CURSOR = "res://assets/images/floor.png"
const PLACEABLE_TURRET_CURSOR = "res://assets/images/placeable_turret_small.png"
var land_mine_cursor = null
var obstacle_cursor = null
var floor_cursor = null
var placeable_turret_cursor = null

var land_mine_install_mode = false
var obstacle_install_mode = false
var floor_install_mode = false
var placeable_turret_install_mode = false


var PlaceableTurret = preload("res://placeable-turret.tscn")


func _ready():
	if global.mode != "STORY":
		$DialogueUI.hide()
	rune_implement()
#	get_tree().set_pause(true)
#	global = get_node("/root/global")
	#########
	print("Starting new level: " + level_name)
	time = 0.0
	victory = false
	# Add level scene to current scene
	if level_name == "":
		level_name = "level-01"
	var scn = ResourceLoader.load("res://" + level_name + ".tscn")
	level = scn.instance()
	add_child(level)
	move_child(level, 0)
	# Load level waves from .json file
	var file = File.new()
	file.open("res://" + level_name + "-waves.json", File.READ) # eg. level-01-waves.json
	var txt = file.get_as_text()
	var d = parse_json(txt)
	print(d)
	if not d:
		print("ERROR: Failed to parse wave file")
		if level_name == "level-01":
			d = {
				  "waves": [
					{
					  "id" : "1",
					  "intval" : 10.0,
					  "enemies" : [
						{"type": "tank-b", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"}
					  ],
					  "count" : 0,
					  "state" : 0
					},
					{
					  "id" : "2",
					  "intval" : 10.0,
					  "enemies" : [
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"}
					  ],
					  "count" : 0,
					  "state" : 0
					}
				  ]
				}
		elif level_name == "level-02":
			d = {
				  "waves": [
					{
					  "id" : "1",
					  "intval" : 10.0,
					  "enemies" : [
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"}
					  ],
					  "count" : 0,
					  "state" : 0
					},
					{
					  "id" : "2",
					  "intval" : 10.0,
					  "enemies" : [
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"}
					  ],
					  "count" : 0,
					  "state" : 0
					},
				   {
					  "id" : "3",
					  "intval" : 10.0,
					  "enemies" : [
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-02"}
					  ],
					  "count" : 0,
					  "state" : 0
					}
				  ]
				}
		elif level_name == "level-03":
			d = {
				  "waves": [
					{
					  "id" : "1",
					  "intval" : 10.0,
					  "enemies" : [
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-01"}
					  ],
					  "count" : 0,
					  "state" : 0
					},
					{
					  "id" : "2",
					  "intval" : 10.0,
					  "enemies" : [
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"},
						{"type": "tank-a", "intval": 2.0, "path": "WavePath-02"}
					  ],
					  "count" : 0,
					  "state" : 0
					},
				   {
					  "id" : "3",
					  "intval" : 10.0,
					  "enemies" : [
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-03"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-04"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-03"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-04"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-03"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-04"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-03"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-04"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-03"},
						{"type": "tank-a", "intval": 1.0, "path": "WavePath-04"}
					  ],
					  "count" : 0,
					  "state" : 0
					}
				  ]
				}
	waves = d["waves"]
	# Initialize first wave
	init_wave(0)
	get_node("CanvasLayer/WaveSprite/WaveLabel").set_text(str(wave_idx+1) + "/" + str(waves.size()))
	# Enable process functions
	set_process(true)
	set_process_input(true)
	# Create mouse cursors
	arrow_cursor = load(ARROW_CURSOR)
	land_mine_cursor = load(LAND_MINE_CURSOR)
	obstacle_cursor = load(OBSTACLE_CURSOR)
	floor_cursor = load(FLOOR_CURSOR)
	placeable_turret_cursor = load(PLACEABLE_TURRET_CURSOR)
	Input.set_custom_mouse_cursor(arrow_cursor)

func rune_implement():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var rand_num = rng.randi_range(1, 3)
	if global.mode == "STORY":
		get_tree().set_pause(true)
	if rand_num == 1:
		global.health -= 1
		$TouchCamera2D/Rune/AnimationPlayer.play("Rune1")
	elif rand_num == 2:
		global.health += 1
		$TouchCamera2D/Rune/AnimationPlayer.play("Rune2")
	elif rand_num == 3:
		global.cash -= 2
		$TouchCamera2D/Rune/AnimationPlayer.play("Rune3")
		
#	return "level-0" + str(rand_num) 

func init_wave(idx):
	# Initialize current wave
	print(waves)
	wave_idx = idx
	wave = waves[idx]
	wave["count"] = wave["enemies"].size()
	wave["state"] = WAIT
	wave["idx"] = 0
	start_countdown = wave["intval"]
	get_node("CanvasLayer/SkullButton/Label").set_text(str(ceil(start_countdown)) + "s")
	get_node("CanvasLayer/SkullButton").show()


func start_wave():
	# Start current wave: game will start spawning enemies from this wave
	wave["state"] = ACTIVE
	get_node("CanvasLayer/WaveSprite/WaveLabel").set_text(str(wave_idx+1) + "/" + str(waves.size()))
	spawn_time = time + wave["enemies"][wave["idx"]]["intval"]
	get_node("CanvasLayer/SkullButton").hide()


func _process(delta):
#	print(str(global.current_level.level_name.split('-')[-1])) #this gets level number in str
	#$CashSprite.set_global_position($TouchCamera2D.get_camera_position() + Vector2(-100, -250))
	#$HealthSprite.set_global_position($TouchCamera2D.get_camera_position() + Vector2(0, -250))
	#$WaveSprite.set_global_position($TouchCamera2D.get_camera_position() + Vector2(100, -250))
	#$SkullButton.set_global_position($TouchCamera2D.get_camera_position() + Vector2(0, -150))
	time += delta
	if wave["state"] == ACTIVE:
		if wave["idx"] < wave["count"]:
			if time > spawn_time:
				# Spawn next enemy in waves
				# (idx is already incremented)
				var enemy = wave["enemies"][wave["idx"]]
				var scene = global.enemy_scenes[enemy["type"]]
				var enemy_inst = scene.instance()
				level.add_child(enemy_inst)
				enemy_inst.set_global_position(level.get_node("SpawnPoint1").get_global_position())
				var enemy_inst2 = scene.instance()
				level.add_child(enemy_inst2)
				enemy_inst2.set_global_position(level.get_node("SpawnPoint2").get_global_position())
				if level_name == "level-01":
					var enemy_inst3 = scene.instance()
					enemy_inst3.set_global_position(level.get_node("SpawnPoint3").get_global_position())
					level.add_child(enemy_inst3)
				# var path = PathFollow2D.new()
				# path.set_loop(false)
				# level.get_node(enemy["path"]).add_child(path)
				# path.add_child(enemy_inst)

				if (wave["idx"]+1) < wave["count"]:
					# Next enemy in wave
					wave["idx"] += 1
					spawn_time = time + wave["enemies"][wave["idx"]]["intval"]
				else:
					wave["state"] = FINISHED
					if (wave_idx+1) < waves.size():
						# Next waves
						init_wave(wave_idx+1)
			else:
				pass
		else:
			pass
	elif wave["state"] == WAIT:
		start_countdown -= delta
		if start_countdown > 0:
			get_node("CanvasLayer/SkullButton/Label").set_text(str(ceil(start_countdown)) + "s")
		else:
			start_wave()

	get_node("CanvasLayer/CashSprite/CashLabel").set_text(str(global.cash))
	get_node("CanvasLayer/HealthSprite/HealthLabel").set_text(str(global.health))

	if global.health <= 0:
		print("You loose!")
		set_process(false)
	elif (not victory and
		#waves[waves.size()-1]["state"] == FINISHED and
		wave["state"] == FINISHED and
		get_tree().get_nodes_in_group("enemy").size() == 0):
		print("Victory!")
		victory = true
		global.save_game(int(global.current_level.level_name.split('-')[-1])+1)
		get_node("AnimPlayer").play("Victory")
		set_process(false)


func _input(event):
	if event is InputEventKey:
		if Input.is_action_pressed("pause_game"):
			get_tree().set_pause(true)
			get_node("TouchCamera2D/PausePopupPanel").show()
			return
		if Input.is_action_pressed("quit_game"):
			global.save_game(int(global.current_level.level_name.split('-')[-1]))
			get_tree().quit()
			return
		if Input.is_action_pressed("cheat_cash"):
			global.increase_cash(100)
			return
		if Input.is_action_pressed("ui_accept"):
			get_tree().set_pause(true)
			get_node("TouchCamera2D/PausePopupPanel").show()
			return
	elif event is InputEventMouseButton:
		if event.get_button_index() == BUTTON_LEFT and not event.is_pressed():
			# Letf mouse button up
			if land_mine_install_mode:
				plant_mine(event.get_global_position())
				print("plant mine")
				land_mine_install_mode = false
			elif obstacle_install_mode:
				plant_obstacle(event.get_global_position())
				print("plant obstacle")
				obstacle_install_mode = false
			elif floor_install_mode:
				plant_floor(event.get_global_position())
				print("plant floor")
				floor_install_mode = false
			elif placeable_turret_install_mode:
				plant_placeable_turret(event.get_global_position())
				placeable_turret_install_mode = false


func _on_resume_button_pressed():
	get_node("TouchCamera2D/PausePopupPanel").hide()
	get_tree().set_pause(false)


func _on_SkullButton_pressed():
	start_wave()

func gameover_pause():
	get_tree().set_pause(true)


func _on_RestartButton_pressed():
	print(str(level_name))
	get_tree().set_pause(false)
	get_node("/root/global").goto_scene("res://game.tscn", str(level_name))


func _on_ContinueButton_pressed():
	print("Load next level")
	get_tree().set_pause(false)
	# Hack
	var next_level = get_node("/root/global").get_next_level()
	get_node("/root/global").goto_scene("res://game.tscn", next_level)


func _on_QuitButton_pressed():
#	global.save_game(int(global.current_level.level_name.split('-')[-1]))
	get_tree().quit()


func _on_BuyLandMineButton_button_up():
	print("Buy mine")
	if global.init_land_mine_cost <= global.cash:
		print("I'm rich enough")
#		global.decrease_cash(global.init_land_mine_cost)
		global.decrease_cash(500)
		land_mine_install_mode = true
		Input.set_custom_mouse_cursor(land_mine_cursor)


func plant_mine(pos):
	print("plant land mine", pos)
#	var scene = preload("res://land-mine.tscn")
	var scene = preload("res://towerbase.tscn")
	var mine = scene.instance()
	mine.set_position(pos)
	global.current_level.level.add_child(mine)
	Input.set_custom_mouse_cursor(arrow_cursor)
	

func _on_BuyObstacleButton_button_up():
	obstacle_install_mode = true
	Input.set_custom_mouse_cursor(obstacle_cursor, 0, Vector2(25, 25))
	
	
func plant_obstacle(pos):
	var tile_map = self.get_child(0).get_node("Navigation2D/TileMap")
	# print(global.current_level.level)
	print("planting obstacle at " + str(pos))
	self.get_child(0).get_node("Navigation2D/TileMap").set_cellv(self.get_child(0).get_node("Navigation2D/TileMap").world_to_map(pos), 1)
	# print("planting obstacle at " + str(pos))
	Input.set_custom_mouse_cursor(arrow_cursor)


func _on_BuyFloorButton_button_up():
	floor_install_mode = true
	Input.set_custom_mouse_cursor(floor_cursor, 0, Vector2(25, 25))
	

func plant_floor(pos):
	var tile_map = self.get_child(0).get_node("Navigation2D/TileMap")
	print("planting obstacle at " + str(pos))
	self.get_child(0).get_node("Navigation2D/TileMap").set_cellv(self.get_child(0).get_node("Navigation2D/TileMap").world_to_map(pos), 0)
	Input.set_custom_mouse_cursor(arrow_cursor)


func _on_BuyPlaceableTurretButton_button_up():
	placeable_turret_install_mode = true
	Input.set_custom_mouse_cursor(placeable_turret_cursor)
	
	
func plant_placeable_turret(pos):
	var turret = PlaceableTurret.instance()
	turret.set_global_position(pos)
	self.get_child(0).add_child(turret)
	Input.set_custom_mouse_cursor(arrow_cursor)



func _on_DialogueUI_exited():
	get_tree().set_pause(false)


func _on_Button2_pressed():
	global.save_game(int(global.current_level.level_name.split('-')[-1]))


func _on_Game_gui_input(event):
	$TouchCamera2D._unhandled_input(event)
