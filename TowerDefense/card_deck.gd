extends Node2D

onready var global = get_node("/root/global")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.set_global_position(Vector2(600, 600))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_options_pressed():
	$options.hide()
	$decks.show()


func _on_goback_pressed():
	$options.show()
	$decks.hide()


func _on_speargirl_pressed():
#	print(global.current_level.get_child(0).get_name())
	$decks.hide()
	global.deck = "spear"
	global.current_level.get_child(0).get_node("background").modulate = Color(1,1,1,0.5)
	global.button_towerbase(true)
	$goback2.show()


func _on_goback2_pressed():
	$goback2.hide()
	global.current_level.get_child(0).get_node("background").modulate = Color(1,1,1,1)
	global.button_towerbase(false)
	$decks.show()
	


func _on_magician1_pressed():
	global.deck = "mag1"
	$decks.hide()
	global.current_level.get_child(0).get_node("background").modulate = Color(1,1,1,0.5)
	$goback2.show()


func _on_magician2_pressed():
	global.deck = "mag2"
	$decks.hide()
	global.current_level.get_child(0).get_node("background").modulate = Color(1,1,1,0.5)
	$goback2.show()


func _on_bowgirl_pressed():
	global.deck = "bow"
	$decks.hide()
	global.current_level.get_child(0).get_node("background").modulate = Color(1,1,1,0.5)
	$goback2.show()
