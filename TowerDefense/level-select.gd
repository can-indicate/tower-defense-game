extends Node2D

onready var global = get_node("/root/global")

func setup_level_buttons(max_level):
#	var pos = get_child(0).get_global_position()
	for i in range (1, max_level+1):
		var node = "TextureButton" + str(i)
		get_node("levelselection/"+node).show()
#		var button = Button.new()
#		button.text = "Level " + str(i)
#		button.set_global_position(pos + Vector2(0.0, 0.0 + i*30))
#		button.connect("pressed", self, "_on_button_pressed", [i])
#		add_child(button)

#func _on_button_pressed(button_index):
#	get_node("/root/global").goto_scene("res://game.tscn", "level-0" + str(button_index))
	

func _on_TextureButton11_pressed():
	queue_free()


func _on_TextureButton1_pressed():
	global.goto_scene("res://game.tscn", "level-01")


func _on_TextureButton2_pressed():
	global.goto_scene("res://game.tscn", "level-02")


func _on_TextureButton3_pressed():
	global.goto_scene("res://game.tscn", "level-03")


func _on_TextureButton4_pressed():
	global.goto_scene("res://game.tscn", "level-04")


func _on_TextureButton5_pressed():
	global.goto_scene("res://game.tscn", "level-05")


func _on_TextureButton6_pressed():
	global.goto_scene("res://game.tscn", "level-06")


func _on_TextureButton7_pressed():
	global.goto_scene("res://game.tscn", "level-07")


func _on_TextureButton8_pressed():
	global.goto_scene("res://game.tscn", "level-08")


func _on_TextureButton9_pressed():
	global.goto_scene("res://game.tscn", "level-09")


func _on_TextureButton10_pressed():
	global.goto_scene("res://game.tscn", "level-010")
