extends Node

var mode
# Degrees per radian
const DEG_PER_RAD = 57.295779513

# Interval of time before a dead unit is removed from the screen
const DEAD_CLEAN_INTVAL = 3.0

const COLOR_GOLD = "#ffda80"
const COLOR_RED  = "#ff4444"

# Game
var deck #spear, bow, mag1(non-continuouse), mag2(continuous)
var current_level = null
var game_won = false

# Player
var reward = 5
var cash = 10000 #check line 57 also for cash and health
var health 

var debug = false
var init_tower_cost     = 5 # Initial tower cost
var init_land_mine_cost = 2 # Initial land mine cost

var enemy_scenes = {}

func _ready():
	var root = get_tree().get_root()
	current_level = root.get_child( root.get_child_count() - 1)
	# Preload enemies' scenes
	enemy_scenes["tank-a"] = preload("res://tank-a.tscn")


func hit_fortress(damage):
	if health > 0:
		decrease_health(damage)
		if health <= 0:
			current_level.get_node("AnimPlayer").play("GameOver")

func button_towerbase(status):
	if status:
		var towerbase = get_tree().get_nodes_in_group("towerbase")
		for i in towerbase:
			i.get_node("button").show()
	if not status:
		var towerbase = get_tree().get_nodes_in_group("towerbase")
		for i in towerbase:
			i.get_node("button").hide()



func get_next_level():
	var level = int(current_level.level_name.split('-')[-1])
	if level == 10 or game_won:
		game_won = true
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var rand_num = rng.randi_range(1, 10)
		return "level-0" + str(rand_num)
	else:
		return "level-0" + str(level + 1)

func print_levelname():
	return current_level.level_name

func goto_scene(scene, level_name=""):
	current_level.queue_free()
#	if scene.get_basename() != "splash.tscn":
#		# Reset player attributes
	if mode == "STORY":
		cash = 1000
		health = 2
	var scn = ResourceLoader.load(scene)
	current_level = scn.instance()
	if level_name != "":
		current_level.level_name = level_name
	get_tree().get_root().add_child(current_level)

func enemy_kill_reward():
	cash += reward
#	get_tree().call_group("CashListeners","on_cash_update")

#func increase_cash(sum):
#	cash += sum
#	# Update listeners
#	get_tree().call_group("CashListeners","on_cash_update")


func decrease_cash(sum):
	cash -= sum
	# Update listeners
#	get_tree().call_group("CashListeners","on_cash_update")


func increase_health(point):
	health += point
	# Update listeners
	get_tree().call_group("HealthListeners","on_health_update")


func decrease_health(point):
	health -= point
	# Update listeners
	get_tree().call_group("HealthListeners","on_health_update")
	
	
func save_game(level):
	var level_name = str(level)
	var save_game = File.new()
	save_game.open("res://game.save", File.WRITE)
	save_game.store_line(to_json(
		{"level": level, 
		"game_won": game_won,
		"cash": cash,
		"health": health
		}))
	

